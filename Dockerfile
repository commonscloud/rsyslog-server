FROM debian:stable-slim

RUN apt update && apt upgrade -y

RUN apt install -y rsyslog

RUN adduser \
   --shell /usr/sbin/nologin \
   --disabled-password \
   rsyslog

COPY rsyslog.conf /etc/rsyslog.conf

WORKDIR /home/rsyslog

USER rsyslog

EXPOSE 514

VOLUME ["/home/rsyslog"]

CMD ["rsyslogd","-n","-iNONE"]
