# Rsyslog Server

RSyslog Server containerized launched as _rsyslog_ username and saving log on _/home/rsyslog_ listening on port 514/tcp

# Build

```
$ docker build . -t rsyslog-server
```

# Execute

```
$ docker run -p 514:514 --rm rsyslog-server
```

# Configure clients

Add or create on _/etc/rsyslog/50-default.conf:_

```

*.* @@127.0.0.1:514

# ... other configs

```

and restart rsyslog

```
$ sudo systemctl restart rsyslog
```

Check server is recording logs:

```
$ docker exec -it rsyslog-server ls -R

.:
your-hostname-rsyslog-client

./your-hostname-rsyslog-client:
dockerd.log  kernel.log  rsyslogd.log  rtkit-daemon.log  sudo.log  systemd.log
